#!/usr/bin/python3
# -*- coding: utf-8 -*-


import re


mystr = './img\\正则表达式1【精选】1.png'

mylist = ['./img\\正则表达式1【精选】1.png',
        './img\\正则表达式1【精选】6.png',
        './img\\正则表达式1【精选】4.png',
        './img\\正则表达式1【精选】5.png',
        './img\\正则表达式1【精选】3.png',
        './img\\正则表达式1【精选】2.png'
]


url_list = [
        'https://wenku.baidu.com/view/9b98ce91cd1755270722192e453610661ed95aaa.html?from=search',
        'https://wenku.baidu.com/view/f736cafadd36a32d72758161.html',
        'https://wenku.baidu.com/view/7e6378fd43323968001c9267.html?from=search',
]

# mylist_sort = sorted(mylist, key=lambda path : int(re.findall('(\d+)\.png', path)[0]))
# test = lambda path : int(re.findall('(\d+)\.png', path)[0])m
# r = re.findall('(\d+)\.png', mystr)
#r = int(re.match(r'(\d+)\.png',mystr).group())
#print(mylist_sort)


# 检查文库链接地址合法性
def check_bdwenku_url(url):
        pattern1 = '^https://wenku.baidu.com/view/.*\.html\?from=search$'
        pattern2 = '^https://wenku.baidu.com/view/.*\.html$'

        # 只要符合一种类型
        if re.findall(pattern1, url) or re.findall(pattern2, url):
                return True
        else:
                return False



for cnt, url in enumerate(url_list, start=1):
        if check_bdwenku_url(url):
                print("url " + url + ' 合法')
        else:
                print('第 %d 条 url 非法'%cnt)
