# 🔥 文档助手
 
### 免券下载百度文库格式为 PPT, DOC, PDF(图片) 的非试看文档
![avatar](/src/gui2.png)

### ✨ 特性
1. 免券下载百度文库PPT, DOC, PDF(图片) 非试看文档
2. 下载文件自动识别转换到相应格式（PPT PDF 保留原始图片 并生成PDF文件）
3. 对下载的文档图片进行图像处理使字体比原始锐利，图像更鲜艳
4. 使用PyQt5 添加GUI
5. 增加URL校验和解决中文乱码


### ⚡ 环境依赖

- Python3.7
- requests 
- BeautifulSoup
- reportlab 
- PIL 
- PyQt5


### ⌛️ 食用方法

- 克隆仓库
```ssh
git clone https://gitee.com/kali20gakki/document_assistant.git
```

- 安装依赖
```ssh
pip3 install -r requirements.txt
```

- 运行
```ssh
python3 run.py
```




### ⚡免责声明
仅限用于学习和研究目的；不得将上述内容用于商业或者非法用途，否则，一切后果请用户自负

