#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys, re
from time import strftime, localtime
from PyQt5.QtGui import QIcon, QPixmap, QFont
from PyQt5.QtWidgets import (QWidget, QDesktopWidget, 
                            QApplication,QMainWindow, 
                            QLabel, QLineEdit,
                            QPushButton, QTextBrowser,
                            QMessageBox
)
from PyQt5.QtCore import QFileInfo, Qt



class wenkuUI(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()


    def initUI(self):               

        self.resize(900, 430)
        self.setWindowTitle('百度文库助手v0.1(@Mrtutu)')

        root = QFileInfo(__file__).absolutePath()
        self.setWindowIcon(QIcon(root + '/src/icon.jpg')) 

        self.logolbl = QLabel('logo',self)
        self.logolbl.resize(250,150)
        self.logolbl.setScaledContents(True)
        self.logolbl.move(320,-15)
        self.logolbl.setPixmap(QPixmap(root + "/src/logo.png"))

        self.urllbl = QLabel('文档URL:',self)
        font = QFont('Consolas', 13, -25)
        self.urllbl.setFont(font)
        self.urllbl.move(37,133)



        self.urlinput = QLineEdit(self)
        self.urlinput.resize(600,40)
        self.urlinput.move(150,130)
        self.urlinput.setPlaceholderText("支持非试看版 PPT DOC PDF")

        self.downloadbtn = QPushButton('下 载',self)
        self.downloadbtn.resize(80,42)
        self.downloadbtn.move(770,129)

        self.downloadbtn.clicked.connect(self.btnClik)

        self.tbr = QTextBrowser(self)
        self.tbr.resize(600,180)
        self.tbr.move(150,200)
        self.tbr.setPlaceholderText('Log 输出')
        

        self.setWindowFlags(Qt.WindowCloseButtonHint)
        self.setFixedSize(self.width(), self.height())

        self.center() 
        self.show()


    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())
    
    def printlog(self, msg):
        tstr = strftime("[%H:%M:%S]: ", localtime())
        self.tbr.append(tstr + msg)
        self.cursor = self.tbr.textCursor()
        self.tbr.moveCursor(self.cursor.End)#光标移到最后，这样就会自动显示出来
        QApplication.processEvents() #一定加上这个功能，不然有卡顿

    def btnClik(self):
        #self.printlog(self.urlinput.text())
        self.tbr.clear()
        # URL 校验正确
        if check_bdwenku_url(self.urlinput.text()):
            self.printlog('URL 格式正确...')
        elif self.urlinput.text() == '':
            QMessageBox.information(self, '错误', '请输入URL :)')
        else:
            QMessageBox.information(self, '错误', '此URL格式错误！\n请检查:)')



# 检查文库链接地址合法性
def check_bdwenku_url(url):
        pattern1 = '^https://wenku.baidu.com/view/.*\.html\?from=search$'
        pattern2 = '^https://wenku.baidu.com/view/.*\.html$'

        # 只要符合一种类型
        if re.findall(pattern1, url) or re.findall(pattern2, url):
                return True
        else:
                return False

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = wenkuUI()
    sys.exit(app.exec_())